﻿using System;
using System.Windows.Forms;
using Microsoft.Speech.Recognition;
using System.Speech.Synthesis;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Threading;
using System.Reflection;
using System.Data.OleDb;
using System.Drawing;
using WindowsFormsApplication372;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public static Keys ConvertFromString(string keystr)
        {
            return (Keys)Enum.Parse(typeof(Keys), keystr);                       
        }
        private static ushort key(string name)
        {
            var dict = new Dictionary<string, ushort>
            {{"`",0x29},{"1",0x02},{"2",0x03 },{"3",0x04 },{"4",0x05 },{"5",0x06 },{"6",0x07 },{"7",0x08 },{"8",0x09 },{"9",0x0A },{"0",0x0B },{"-",0x0C },{"=",0x0D },{"Tab",0x0F },{"q",0x10 },{"w",0x11 },{"e",0x12 },{"r",0x13 },{"t",0x14 },{"y",0x15 },{"u",0x16 },{"i",0x17 },{"o",0x18 },{"p",0x19 },{"[",0x1A },{"]",0x1B },{"a",0x1E },{"s",0x1F },{"d",0x20 },{"f",0x21 },{"g",0x22 },{"h",0x23 },{"j",0x24 },{"k",0x25 },{"l",0x26 },{";",0x27 },{"'",0x28 },{"\\",0x2B},{"z",0x2C },{"x",0x2D },{"c",0x2E },{"v",0x2F },{"b",0x30 },{"n",0x31 },{"m",0x32 },{",",0x33 },{".",0x34 },{"/",0x35 },{"f1",0x3b },{"f2",0x3c },{"f3",0x3d },{"f4",0x3e },{"f5",0x3f },{"f6",0x40 },{"f7",0x41 },{"f8",0x42 },{"f9",0x43 },{"f10",0x44 },{"f11",0x57 },{"f12",0x58 },{"Scroll lock",0x46 },{"esc",0x01 },{"return",0x1c },{"enter",0x1c },{"space",0x39 },{" ",0x39 },{"insert",0xd2 },{"delete",0xd3 },{"backspace",0x0e },{"left",0xcb },{"right",0xcd },{"up",0xc8 },{"down",0xd0 },{"home",0xc7 },{"end",0xcf },{"page up",0xc9 },{"page down",0xd1 },{"Numlock",0x45 },{"Num_=,",0xd },{"Num_/",0xb5 },{"Num_*",0x37 },{"Num_-",0x4a },{"Num_+",0x4e },{"Num_.",0x53 },{"Num_0",0x52 },{"Num_1",0x4f },{"Num_2",0x50 },{"Num_3",0x51 },{"Num_4",0x4b },{"Num_5",0x4c },{"Num_6",0x4d },{"Num_7",0x47 },{"Num_8",0x48 },{"Num_9",0x49 },{"Num_enter",0x9c },{"а", 0x21 },{"б", 0x33 },{"в", 0x20 },{"г", 0x16 },{"д", 0x26 },{"е", 0x14 },{"ё", 0x29},{"ж", 0x27 },{"з", 0x19 },{"й", 0x10 },{"и", 0x30 },{"к", 0x13 },{"л", 0x25 },{"м", 0x2F },{"н", 0x15 },{"о", 0x24 },{"п", 0x22 },{"р", 0x23 },{"с", 0x2E },{"т", 0x31 },{"у", 0x12 },{"ф", 0x1E},{"х", 0x1A },{"ц", 0x11 },{"ч", 0x2D },{"ш", 0x17 },{"щ", 0x18 },{"ъ", 0x1B },{"ы", 0x1F },{"ь", 0x32 },{"э", 0x28 },{"ю", 0x34 },{"я", 0x2C }};

            return dict[name];
        }

        class CMD
        {                             
            public void wait (string ms) { Thread.Sleep(Convert.ToInt32(ms.Replace(" ", ""))); }

            public void send(string name_of_key) { send_down(name_of_key); Thread.Sleep(100); send_up(name_of_key);  }
            public void send_down(string name_of_key) { KeyBoardSimulator.SendKey(key(name_of_key), KeyBoardSimulator.KEYEVENTF.KeyDown | KeyBoardSimulator.KEYEVENTF.SCANCODE); }
            public void send_up(string name_of_key) { KeyBoardSimulator.SendKey(key(name_of_key), KeyBoardSimulator.KEYEVENTF.KEYUP | KeyBoardSimulator.KEYEVENTF.SCANCODE); }

            public void wsend(string name_of_key) { wsend_down(name_of_key); Thread.Sleep(100); wsend_up(name_of_key); }
            public void wsend_down(string name_of_key) { KeyBoardSimulator.KeyboardSend.KeyDown(ConvertFromString(name_of_key.Replace(" ", ""))); }
            public void wsend_up(string name_of_key) { KeyBoardSimulator.KeyboardSend.KeyUp(ConvertFromString(name_of_key.Replace(" ", ""))); }

            public void ssend (string name_of_key) { ssend_down(name_of_key); Thread.Sleep(100); ssend_up(name_of_key);}
            public void ssend_down (string name_of_key) { KeyBoardSimulator.SendSpecialKey((int)ConvertFromString(name_of_key.Replace(" ", "")), KeyBoardSimulator.KEYEVENTF.KeyDown | KeyBoardSimulator.KEYEVENTF.SCANCODE); }
            public void ssend_up (string name_of_key) { KeyBoardSimulator.SendSpecialKey((int)ConvertFromString(name_of_key.Replace(" ", "")), KeyBoardSimulator.KEYEVENTF.KEYUP | KeyBoardSimulator.KEYEVENTF.SCANCODE); }
            
            public void say (string text) { char[] chars = text.ToCharArray(); for (int i = 0; i < chars.Length; i++) { send(chars[i].ToString()); } }
            public void run (string path) { System.Diagnostics.Process.Start(@path); }     
                
            public void mouse_move_abs(string xy){ xy = xy.Replace(" ", ""); int x = Convert.ToInt32(xy.Substring(0, xy.IndexOf(","))); int y = Convert.ToInt32(xy.Substring(xy.IndexOf(",") + 1, xy.Length - xy.IndexOf(",") - 1)); MouseSimulator.absMouseMove(CalculateAbsoluteCoordinateX(x), CalculateAbsoluteCoordinateY(y));}   
            public void mouse_move (string xy){ xy = xy.Replace(" ", "");    int x = Convert.ToInt32(xy.Substring(0, xy.IndexOf(","))); int y = Convert.ToInt32(xy.Substring(xy.IndexOf(",") + 1, xy.Length - xy.IndexOf(",") - 1));x = Cursor.Position.X + x;y = Cursor.Position.Y + y;MouseSimulator.absMouseMove(CalculateAbsoluteCoordinateX(x), CalculateAbsoluteCoordinateY(y));}

            public void left_click_down(string xy){mouse_move(xy);MouseSimulator.ClickLeftMouseButton_Down();}
            public void left_click_up (string xy){mouse_move(xy);MouseSimulator.ClickLeftMouseButton_Up();}
            public void left_click(string xy) { left_click_down(xy); Thread.Sleep(100); left_click_up(xy); }

            public void right_click_down (string xy){mouse_move(xy);MouseSimulator.ClickRightMouseButton_Down();}
            public void right_click_up(string xy) { mouse_move(xy); MouseSimulator.ClickRightMouseButton_Up(); }
            public void right_click(string xy) { right_click_down(xy); Thread.Sleep(100); right_click_up(xy); }

            public void MIDDLE_click_down(string xy) { mouse_move(xy); MouseSimulator.ClickMIDDLEMouseButton_Down(); }
            public void MIDDLE_click_up(string xy) { mouse_move(xy); MouseSimulator.ClickMIDDLEMouseButton_Up(); }
            public void MIDDLE_click(string xy) { MIDDLE_click_down(xy); Thread.Sleep(100); MIDDLE_click_up(xy); }
			
			public void X_click_down(string xy) { mouse_move(xy); MouseSimulator.ClickXMouseButton_Down(); }
            public void X_click_up(string xy) { mouse_move(xy); MouseSimulator.ClickXMouseButton_Up(); }
            public void X_click(string xy) { X_click_down(xy); Thread.Sleep(100); X_click_up(xy); }

            public void left_click_down_abs(string xy) { mouse_move_abs(xy); MouseSimulator.ClickLeftMouseButton_Down(); }
            public void left_click_up_abs(string xy) { mouse_move_abs(xy); MouseSimulator.ClickLeftMouseButton_Up(); }
            public void left_click_abs(string xy) { left_click_down_abs(xy); Thread.Sleep(100); left_click_up_abs(xy); }

            public void right_click_down_abs(string xy) { mouse_move_abs(xy); MouseSimulator.ClickRightMouseButton_Down(); }
            public void right_click_up_abs(string xy) { mouse_move_abs(xy); MouseSimulator.ClickRightMouseButton_Up(); }
            public void right_click_abs(string xy) { right_click_down_abs(xy); Thread.Sleep(100); right_click_up_abs(xy); }

            public void move_to_pic(string path) {Point img = screen_search(path);  string xy = img.X.ToString() + "," + img.Y.ToString(); mouse_move_abs(xy); }

            public void cycle_end(string temp) { cycle = false;/*global_cmd = "";*/ }    
            public void cycle_start(string temp) { cycle = true;/*parser(global_cmd);*/ }

            public void voice(string text) { synth.Speak(text); }
        }

        enum SystemMetric
        {
            SM_CXSCREEN = 0,
            SM_CYSCREEN = 1,
        }

        [DllImport("user32.dll")]
        static extern int GetSystemMetrics(SystemMetric smIndex);

        static int CalculateAbsoluteCoordinateX(int x)
        {
            return (x * 65536) / GetSystemMetrics(SystemMetric.SM_CXSCREEN);
        }

        static int CalculateAbsoluteCoordinateY(int y)
        {
            return (y * 65536) / GetSystemMetrics(SystemMetric.SM_CYSCREEN);
        }        

        public Form1()
        {
            InitializeComponent();
        }

        static string global_cmd;
        async void sre_SpeechRecognized(object sender, SpeechRecognizedEventArgs e)
        {
            if (e.Result.Confidence > Convert.ToDouble(toolStripTextBox1.Text))
            {
                
                for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                {
                    if (dataGridView1[0, i].Value.ToString() == e.Result.Text)
                    {                        
                        global_cmd = dataGridView1[2, i].Value.ToString();
                        if (global_cmd.Length > 0) { cycle = false; await Task.Run(() => parser(global_cmd)); }
                        try
                        {
                            if (dataGridView1[1, i].Value.ToString() != null && dataGridView1[1, i].Value.ToString() != "")
                            {
                                await Task.Run(() => synth.Speak(dataGridView1[1, i].Value.ToString()));
                            }
                        }
                        catch { }                        
                        break;
                    }
                }
            }
        }

        static bool cycle = false;
        static public async void  parser(string cmd)
        {
            int i=0; string error_func="";
            try
            {
                CMD type; string p_cmd; string att_cmd; string name_cmd;
                int count = cmd.Split('\r').Length;
                int count_of_cycles;

                if (cmd.IndexOf("\r") == -1) { p_cmd = cmd.Substring(0, cmd.Length); } else { p_cmd = cmd.Substring(0, cmd.IndexOf("\r")); }
                try { name_cmd = p_cmd.Substring(0, p_cmd.IndexOf(" ")).Replace(" ", ""); } catch { name_cmd = p_cmd.Substring(0, p_cmd.Length).Replace(" ", ""); }
                try { att_cmd = p_cmd.Substring(p_cmd.IndexOf(" ") + 1, p_cmd.Length - p_cmd.IndexOf(" ") - 1); } catch { att_cmd = ""; }
                try { count_of_cycles = Convert.ToInt16(att_cmd); } catch { count_of_cycles = -1; }
                if (name_cmd == "cycle_start")
                {
                    cycle = true;
                    global_cmd = global_cmd.Remove(0, cmd.IndexOf("\r") + 2);
                    int number_of_cycle_now = 0;
                    while (cycle && count_of_cycles!=number_of_cycle_now)
                    {
                        cmd = global_cmd;
                        count = cmd.Split('\r').Length;
                        for (i = 0; i < count; i++)
                        {
                            if (cycle == false) { return; }
                            if (cmd.IndexOf("\r") == -1) { p_cmd = cmd.Substring(0, cmd.Length); } else { p_cmd = cmd.Substring(0, cmd.IndexOf("\r")); }
                            cmd = cmd.Remove(0, cmd.IndexOf("\r") + 2);
                            try { name_cmd = p_cmd.Substring(0, p_cmd.IndexOf(" ")).Replace(" ", ""); } catch { name_cmd = p_cmd.Substring(0, p_cmd.Length).Replace(" ", ""); }
                            try { att_cmd = p_cmd.Substring(p_cmd.IndexOf(" ") + 1, p_cmd.Length - p_cmd.IndexOf(" ") - 1); } catch { att_cmd = ""; }
                            error_func = p_cmd;
                            type = new CMD();
                            MethodInfo info = type.GetType().GetMethod(name_cmd);
                            await Task.Run(() => info.Invoke(type, new object[] { att_cmd }));
                        }
                        number_of_cycle_now++;
                    }
                }
                else
                {
                    for (i = 0; i < count; i++)
                    {
                        if (cmd.IndexOf("\r") == -1) { p_cmd = cmd.Substring(0, cmd.Length); } else { p_cmd = cmd.Substring(0, cmd.IndexOf("\r")); }
                        cmd = cmd.Remove(0, cmd.IndexOf("\r") + 2);
                        try { name_cmd = p_cmd.Substring(0, p_cmd.IndexOf(" ")).Replace(" ", ""); } catch { name_cmd = p_cmd.Substring(0, p_cmd.Length).Replace(" ", ""); }
                        try { att_cmd = p_cmd.Substring(p_cmd.IndexOf(" ") + 1, p_cmd.Length - p_cmd.IndexOf(" ") - 1); } catch { att_cmd = ""; }
                        error_func = p_cmd;
                        type = new CMD();
                        MethodInfo info = type.GetType().GetMethod(name_cmd);
                        await Task.Run(() => info.Invoke(type, new object[] { att_cmd }));
                    }
                }
            }
            catch { MessageBox.Show("Ошибка в макросе \r\n" + i.ToString() + ": " + error_func); }

        }
        
        static SpeechSynthesizer synth;
        SpeechRecognitionEngine sre;

        private void reg_new_cmd()
        {
            try
            {
                if (dataGridView1.RowCount > 1)
                {
                    sre.RecognizeAsyncCancel();
                    sre.UnloadAllGrammars();
                    Choices numbers = new Choices();
                    for (int i = 0; i < dataGridView1.RowCount - 1; i++)
                    {
                        numbers.Add(dataGridView1[0, i].Value.ToString());
                    }

                    GrammarBuilder gb = new GrammarBuilder();
                    gb.Append(numbers);
                    Grammar g = new Grammar(gb);
                    sre.LoadGrammar(g);

                    sre.RecognizeAsync(RecognizeMode.Multiple);
                }
            }
            catch { MessageBox.Show("Произошла ошибка, попробуйте еще раз"); }
        }

        OleDbConnection connP = new OleDbConnection(); // Создаём объект соединения

        private void connect_to_db(string dir_name)
        {
            connP.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;User ID=;Data Source=" + dir_name; // Строка коннекта к датабазе            
            try
            {
                //пробуем подключится
                connP.Open(); // Открытие подключения к Access
                //this.Text = "БД Подключена";
            }
            catch (OleDbException se)
            {
                MessageBox.Show("Ошибка подключения к БД" + se.Message);
                return;
            }
        }

        private void disconnect_from_db()
        {
            connP.Close();
            connP.Dispose();
        }

        private string get_option(string name)
        {
            OleDbCommand commP = connP.CreateCommand();  // Создаём команндера
            commP.CommandText = "select top 1 ("+name+") from Настройки";
            commP.ExecuteNonQuery(); //выполняем действие
            OleDbDataReader reader = commP.ExecuteReader();
            reader.Read();
            return reader.GetString(0).ToString();
        }

        private void get_profiles()
        {
            OleDbCommand commP = connP.CreateCommand();  // Создаём команндера
            commP.CommandText = "select Название from Профили";
            commP.ExecuteNonQuery(); //выполняем действие
            OleDbDataReader reader = commP.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    toolStripComboBox2.Items.Add(reader.GetString(0));
                }
            } 
        }

        private void load_options()
        {
            //распознание
            sre = new SpeechRecognitionEngine(new System.Globalization.CultureInfo("ru-RU"));
            sre.SetInputToDefaultAudioDevice();
            sre.SpeechRecognized += new EventHandler<SpeechRecognizedEventArgs>(sre_SpeechRecognized);
            //говорение
            synth = new SpeechSynthesizer();
            List<InstalledVoice> installedVoices = new List<InstalledVoice>();
            foreach (InstalledVoice voice in synth.GetInstalledVoices())
            {
                installedVoices.Add(voice);
                toolStripComboBox1.Items.Add(voice.VoiceInfo.Name);
            }
            connect_to_db("save.mdb");
            get_profiles();
            toolStripComboBox1.SelectedItem = get_option("Голос");
            toolStripComboBox2.SelectedItem = get_option("Профиль");
            toolStripTextBox1.Text = get_option("Точность");
            toolStripTextBox2.Text = get_option("Скорость");
            toolStripTextBox3.Text = get_option("Громкость");            
        }

        private void save_options()
        {
            try
            {
                OleDbCommand commP = connP.CreateCommand();  // Создаём команндера
                commP.CommandText = "UPDATE  Настройки SET Голос='" + toolStripComboBox1.SelectedItem.ToString() + "', Профиль='" + toolStripComboBox2.SelectedItem.ToString() + "', Точность='" + toolStripTextBox1.Text + "', Скорость='" + toolStripTextBox2.Text + "', Громкость='" + toolStripTextBox3.Text + "' WHERE Код=3;";
                commP.ExecuteNonQuery(); //выполняем действие
            } catch { MessageBox.Show("Настройки не могут быть сохранены \r\nПроверьте правильность"); }
        }

        Редактор редактор;
        private void Form1_Load(object sender, EventArgs e)
        {
            load_options();
            редактор = new Редактор();
            редактор.Owner = this;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            notifyIcon1.Visible = false;
            save_options();
            disconnect_from_db();
        }

        private void обновитьКомандыToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reg_new_cmd();
        }

        private void сохранитьНастройкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save_options();
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            настройкиToolStripMenuItem.HideDropDown();
            try
            {
                synth.SelectVoice(toolStripComboBox1.SelectedItem.ToString());
            } catch { MessageBox.Show("Ошибка выбор голоса, выберите другой"); }
        }

        private void toolStripComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            настройкиToolStripMenuItem.HideDropDown();
            int n = 0;
            OleDbCommand commP = connP.CreateCommand();  // Создаём команндера
            commP.CommandText = "SELECT Команда, Отклик, Макрос FROM " + "["+ toolStripComboBox2.SelectedItem.ToString()+"]";
            commP.ExecuteNonQuery(); //выполняем действие
            OleDbDataReader reader = commP.ExecuteReader();
            dataGridView1.RowCount = 1;
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    dataGridView1.RowCount++;
                    try { dataGridView1[0, n].Value = reader.GetString(0); } catch { }
                    try { dataGridView1[1, n].Value = reader.GetString(1); } catch { }
                    try { dataGridView1[2, n].Value = reader.GetString(2); } catch { }
                    n++;
                }
            }

            dataGridView1.ClearSelection();
            reg_new_cmd();
        }

        private void toolStripTextBox2_TextChanged(object sender, EventArgs e)
        {
            try { synth.Rate = Convert.ToInt16(toolStripTextBox2.Text); } catch { }
        }

        private void toolStripTextBox3_TextChanged(object sender, EventArgs e)
        {
            try { synth.Volume = Convert.ToInt16(toolStripTextBox3.Text); } catch { }
        }

        private void toolStripTextBox5_Click(object sender, EventArgs e)
        {
            OleDbCommand commP = connP.CreateCommand();  // Создаём команндера
            commP.CommandText = "INSERT INTO Профили(Название) VALUES ('"+toolStripTextBox4.Text+"');";
            commP.ExecuteNonQuery(); //выполняем действие
            commP.CommandText = "create table [" + toolStripTextBox4.Text + "](Команда text WITH COMPRESSION,  Отклик text WITH COMPRESSION,  Макрос text WITH COMPRESSION); ";
            commP.ExecuteNonQuery();//выполняем действие
            toolStripComboBox2.Items.Clear();
            get_profiles();
            toolStripTextBox4.Text = "";
        }

        private void toolStripTextBox6_Click(object sender, EventArgs e)
        {
            disconnect_from_db();
            connect_to_db("save.mdb");
            OleDbCommand commP = connP.CreateCommand();  // Создаём команндера
            commP.CommandText = "DROP TABLE [" + toolStripTextBox4.Text + "];";
            commP.ExecuteNonQuery(); //выполняем действие
            commP.CommandText = "DELETE FROM Профили WHERE Название='" + toolStripTextBox4.Text + "';";
            commP.ExecuteNonQuery(); //выполняем действие
            toolStripComboBox2.Items.Clear();
            get_profiles();
            toolStripTextBox4.Text = "";
        }

        private void save_profile()
        {
            disconnect_from_db();
            connect_to_db("save.mdb");
            OleDbCommand commP = connP.CreateCommand();  // Создаём команндера
            commP.CommandText = "DELETE FROM [" + toolStripComboBox2.SelectedItem.ToString() + "];";
            commP.ExecuteNonQuery(); //выполняем действие

            string cmd, response, macros;
            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                try { cmd = dataGridView1[0, i].Value.ToString(); } catch { cmd = ""; }
                try { response = dataGridView1[1, i].Value.ToString(); } catch { response = ""; }
                try { macros = dataGridView1[2, i].Value.ToString(); } catch { macros = ""; }
                commP.CommandText = "INSERT INTO [" + toolStripComboBox2.SelectedItem.ToString() + "](Команда, Отклик, Макрос) VALUES ('" + cmd + "','" + response + "','" + macros + "');";
                commP.ExecuteNonQuery(); //выполняем действие
            }
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save_profile();
            reg_new_cmd();
        }

        static private Point screen_search(string path)
        {
            Template template;
            Bitmap source;
            Point? foundPoint;
            //загружаем изображение
            source = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Graphics graphics = Graphics.FromImage(source as Image);
            graphics.CopyFromScreen(0, 0, 0, 0, source.Size);

            //уст стандартное DPI
            source.SetResolution(96, 96);

            //берем фрагмент из большого изображения
            var fragment = new Bitmap(@path);

            //создаем шаблон
            template = new Template(fragment);

            //поиск точки
            foundPoint = template.Find(source);
            //центр найденного фрагмента
            Point center = new Point(foundPoint.Value.X + fragment.Width / 2, foundPoint.Value.Y + fragment.Height / 2);  

            return center;
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            if (notifyIcon1.Visible == false)
            {
                this.ShowInTaskbar = false;
                notifyIcon1.Visible = true;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                this.ShowInTaskbar = true;
                notifyIcon1.Visible = false;
            }
        }

        private void выходToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                this.WindowState = FormWindowState.Normal;
                this.ShowInTaskbar = true;
                notifyIcon1.Visible = false;
            }
        }

        public int column_cell_clicked_index, row_cell_clicked_index;
        private void open_editor(int ColumnIndex, int RowIndex)
        {
            редактор.textBox1.Text = dataGridView1[ColumnIndex, RowIndex].Value.ToString();
            редактор.textBox1.SelectionStart = редактор.textBox1.Text.Length;
            редактор.ShowDialog();
        }

        int DropDownWidth(ToolStripComboBox myCombo)
        {
            int maxWidth = 0, temp = 0;
            foreach (var obj in myCombo.Items)
            {
                temp = TextRenderer.MeasureText(obj.ToString(), myCombo.Font).Width;
                if (temp > maxWidth)
                {
                    maxWidth = temp;
                }
            }
            return maxWidth;
        }

        private void toolStripTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                настройкиToolStripMenuItem.HideDropDown();
            }
        }

        private void toolStripTextBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                настройкиToolStripMenuItem.HideDropDown();
            }
        }

        private void toolStripTextBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                настройкиToolStripMenuItem.HideDropDown();
            }
        }

        private void dataGridView1_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                column_cell_clicked_index = e.ColumnIndex; row_cell_clicked_index = e.RowIndex;
                if (dataGridView1[e.ColumnIndex, e.RowIndex].Value == null) { if (e.ColumnIndex == 0) { dataGridView1.RowCount++; dataGridView1.ClearSelection(); dataGridView1[e.ColumnIndex, e.RowIndex].Selected = true; dataGridView1[e.ColumnIndex, e.RowIndex].Value = ""; dataGridView1[e.ColumnIndex + 1, e.RowIndex].Value = ""; dataGridView1[e.ColumnIndex + 2, e.RowIndex].Value = ""; open_editor(e.ColumnIndex, e.RowIndex); } else { MessageBox.Show("Сначала заполните поле Команда"); } }
                else { open_editor(e.ColumnIndex, e.RowIndex); }
            } catch { MessageBox.Show("Что-то пошло не так при добавление новой команды"); }

        }
    }
}