﻿using HotKey_ns;
using System;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Редактор : Form
    {
        public Редактор()
        {
            InitializeComponent();
        }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1 main = this.Owner as Form1;
            if (main != null)
            {
                main.dataGridView1[main.column_cell_clicked_index, main.row_cell_clicked_index].Value = textBox1.Text;
            }
            this.Hide();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                if (sender != null)
                    ((TextBox)sender).SelectAll();
            }
        }

        private void bind()
        {
            var hkey = new HotKey(Keys.A, KeyModifiers.Control);
            hkey.Pressed += (o, e) =>
            {
                button1.Text = Cursor.Position.X + "," + Cursor.Position.Y;
                Clipboard.SetText(button1.Text);
                button1.Enabled = true;
                hkey.Unregister();
            };
            hkey.Register(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            bind();
            button1.Enabled = false;
        }
    }
}
